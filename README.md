Obarun is a small C library used by [obarun.org](https://web.obarun.org) projects.

License
---

ISC license

## Installation

See the INSTALL.md file for oblibs upstream compile/build notes.

## Recommended build instructions

The recommended method to build this package directly from git.
```
gbp clone https://gitlab.com/init-diversity/s6-66/oblibs.git && 
cd oblibs && 
gbp buildpackage -uc -us
```
The following should get you all the software required to build using this method:

`sudo apt install git-buildpackage autotools-dev s6 skalibs-dev libs6-dev libexecline-dev execline libexecline2.9 libskarnet2.14 lowdown`


Contact information
-------------------

* Email:
  Eric Vidal `<eric@obarun.org>`

* Mailing list
  https://obarun.org/mailman/listinfo/66_obarun.org/

* Web site:
  https://web.obarun.org/

* XMPP Channel:
  obarun@xmpp.obarun.org

Supports the project
---------------------

Please consider to make [donation](https://web.obarun.org/index.php?id=18)



